import pygame
import os

pygame.init()
screen = pygame.display.set_mode((400, 300))
done = False
is_blue = True

x = 30
y = 30

clock = pygame.time.Clock()

_image_library = {}


def get_image(path):
        global _image_library
        image = _image_library.get(path)
        if image is None:
                canonicalized_path = path.replace('/', os.sep).replace('\\', os.sep)
                image = pygame.image.load(canonicalized_path)
                _image_library[path] = image
        return image

pygame.mixer.init()
pygame.mixer.music.load('sound/cello.wav')
pygame.mixer.music.play()

while not done:
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        done = True

        screen.fill((255, 255, 255))

        screen.blit(get_image('images/sphere.png'), (130, 130))

        pygame.display.flip()
        clock.tick(60)